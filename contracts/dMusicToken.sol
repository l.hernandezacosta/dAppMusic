pragma solidity ^0.4.17;

import 'openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol';

contract dMusicToken is StandardToken {

	string public name = 'dMusicToken';
	string public symbol = 'dMT';
	uint8 public decimals = 2;
	uint public INITIAL_SUPPLY = 12000;
	uint public amountOfMintedDMTokens;
	address public minter;
	address [] public artists;

	constructor() public {
	  minter = msg.sender;
	  totalSupply_ = INITIAL_SUPPLY;
	  balances[msg.sender] = INITIAL_SUPPLY;
	}

	function mint(address receiver) public {
        if (msg.sender != minter) return;
        balances[receiver] += 1000;
        totalSupply_ += 1000;
				amountOfMintedDMTokens += 1000;
    }

	function transferFee() public {
	        //if (msg.sender != minter) return;

					if (artistExists(msg.sender)) {
						transfer(minter, 1);
						//transferFrom(msg.sender, minter, 1);
						return;
					}
					else {
						artists.push(msg.sender);
						transfer(minter, 5);
						//transferFrom(msg.sender, minter, 1);
						return;
					}
	        //balances[receiver] += 1000;
	        //totalSupply_ += 1000;
	    }

	function artistExists(address artist) public returns(bool) {
		uint arrayLength = artists.length;

			for (uint i = 0; i < arrayLength; i++) {
					if( artists[i] == artist) {
						return true;
					}
				}
			return false;
		}

    function distDMTokensOverArtists() public {
        if (msg.sender != minter) return;

				uint arrayLength = artists.length;
				uint shareableTokens = balances[minter] - INITIAL_SUPPLY -amountOfMintedDMTokens;
				shareableTokens = shareableTokens / arrayLength;

					for (uint i = 0; i < arrayLength; i++) {
							transfer(artists[i], shareableTokens);
						}
    }

		event Paid(uint);
	  function () payable {
	      Paid(msg.value);
				uint dTs = (msg.value) / 100000000000000 / 25;
				balances[msg.sender] += dTs;
				totalSupply_ += dTs;
	  }


}
