WebConsole.keyEvent = function(event)   
{   
  switch(event.keyCode){   
    case 13:   
      var the_shell_command = document.getElementById('command').value;   
      if (the_shell_command) {   
        var the_url = 'exec.php?command=' + escape(the_shell_command);   
        makeHttpRequest(the_url, WebConsole.printResult);   
      }   
       break;   
     default:   
       break;   
   }   
}

WebConsole.printResult = function(result_string)  
{  
  var result_div = document.getElementById('result');  
  var result_array = result_string.split('n');  
  
  var new_command = document.getElementById('command').value;  
  result_div.appendChild(document.createTextNode(new_command));  
  result_div.appendChild(document.createElement('br'));  
  
  var result_wrap, line_index, line;  
  
  for (line_index in result_array) {  
    result_wrap = document.createElement('pre');  
    line = document.createTextNode(result_array[line_index]);  
    result_wrap.appendChild(line);  
    result_div.appendChild(result_wrap);  
    result_div.appendChild(document.createElement('br'));  
  }  
  result_div.appendChild(document.createTextNode(':-> '));  
  
  result_div.scrollTop = result_div.scrollHeight;  
  document.getElementById('command').value = '';  
};