App = {
  web3Provider: null,
  contracts: {},

  init: function() {
    console.log('init');
    return App.initWeb3();
  },

  initWeb3: function() {
    // Initialize web3 and set the provider to the testRPC.
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
      web3 = new Web3(web3.currentProvider);
    } else {
      // set the provider you want from Web3.providers
      App.web3Provider = new Web3.providers.HttpProvider('http://127.0.0.1:7545');
          console.log('7545');

      web3 = new Web3(App.web3Provider);
    }

    return App.initContract();
  },

  initContract: function() {
    $.getJSON('build/contracts/dMusicToken.json', function(data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract.
      var dMusicTokenArtifact = data;
      App.contracts.dMusicToken = TruffleContract(dMusicTokenArtifact);
      $("#contractAddress").html(data.networks["1533472076786"].address);
      // Set the provider for our contract.
      App.contracts.dMusicToken.setProvider(App.web3Provider);

      return App.getBalances();
    });

    return App.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '#transferdMTButton', App.handleTransfer);
    $(document).on('click', '#transferdMTFee', App.handleTransferFeeForUpload);
    $(document).on('click', '#transferButton', App.handleTransferForAudio);
    $(document).on('click', '#mintButton', App.handleTransferForMint);
    $(document).on('click', '#distributeButton', App.handleTransferForDistribution);
  },

  handleTransfer: function(event) {
    event.preventDefault();

    var amount = parseInt($('#TTTransferAmount').val());
    var toAddress = $('#TTTransferAddress').val();


    console.log('Transfer ' + amount + ' dMT to ' + toAddress);

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.transfer(toAddress, amount, {from: account});
      }).then(function(result) {
        $("#LOL").show();
        $("#WEIRD").show();

        alert('Transfer Successful ;D!');



        return App.getBalances();

      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  handleTransferFeeForUpload: function(event) {
    event.preventDefault();

    var amount = 10;
    var toAddress = "0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1";


    console.log('Transfer ' + amount + ' dMT to ' + toAddress);

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.transferFee();
      }).then(function(result) {


        alert('Transfered Fee Successfuly ;D!');



        return App.getBalances();

      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  handleTransferForAudio: function() {
    ///event.preventDefault();

    var amount = 1;
    var toAddress = "0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1";


    console.log('Transfer ' + amount + ' dMT to ' + toAddress);

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.transfer(toAddress, amount, {from: account});
      }).then(function(result) {
        console.log(result);
        $("#transInfo").hide();
        $("#transferButton").hide();
        $("#LOL").show();
        $("#WEIRD").show();
        alert('Transfer Successful ;D!');

        console.log("Button Clicked! HTTP Request triggered!");
        $.get("http://localhost:8088/startPython");

        return App.getBalances();
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  handleTransferForMint: function() {
    ///event.preventDefault();

    var amount = 1;
    var toAddress = "0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1";


    console.log('Transfer ' + amount + ' dMT to ' + toAddress);

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.mint(account);
      }).then(function(result) {
        console.log("Successful minted");
        //location.reload();
        return App.getBalances();

      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  handleTransferForDistribution: function() {
    ///event.preventDefault();

    var amount = 1;
    var toAddress = "0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1";


    console.log("Distribution Process!");

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];


      console.log(accounts[0]);
      console.log("web3 accounts: " + web3.eth.getAccounts());


      

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.distDMTokensOverArtists();
      }).then(function(result) {
        console.log("Successful distributed");
        //location.reload();
        return App.getBalances();

      }).catch(function(err) {
        console.log(err.message);
      });
    });
  },

  getBalances: function() {
    console.log('Getting balances...');

    var dMusicTokenInstance;

    web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];



      console.log("account: " + account);
      console.log(account == '0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1'.toLowerCase());

      if(account == '0x90f8bf6a479f320ead074411a4b0e7944ea8c9c1'.toLowerCase()) {
        console.log("Creator address: ");
        $("#mintdMTs").show();
        $("#distdMTs").show();
        $("#mintButton").show();
        $("#distributeButton").show();
      }

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.balanceOf(account);
      }).then(function(result) {
        balance = result.c[0];

        $('#TTBalance').text(balance);
      }).catch(function(err) {
        console.log(err.message);
      });
    });
  }

};

$(function() {
  $(window).load(function() {
    App.init();
  });
});




$("#mintButton").click(function(){
 //$(this).hide();
  console.log("Button Clicked in app.js")

  web3.eth.getAccounts(function(error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.dMusicToken.deployed().then(function(instance) {
        dMusicTokenInstance = instance;

        return dMusicTokenInstance.balanceOf(account);
      }).then(function(result) {
        balance = result.c[0];

        $('#TTBalance').text(balance);
      }).catch(function(err) {
        console.log(err.message);
      });
    });

});
