import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Upload from '@/components/Upload';
import Profile from '@/components/Profile';
import SearchResult from '@/components/SearchResult';
import Likes from '@/components/Likes';
import LogOut from '@/components/LogOut';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload,
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
    },
    {
      path: '/search_result',
      name: 'SearchResult',
      component: SearchResult,
    },
    {
      path: '/likes',
      name: 'Likes',
      component: Likes,
    },
    {
      path: '/log_out',
      name: 'LogOut',
      component: LogOut,
    },
  ],
});
