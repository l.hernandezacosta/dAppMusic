# dAPP Music

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how VueJS works, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).


# Using Truffle with vuejs


IMPORTANT !! Truffle contracts are compiled to build/contracts. It was necessary to make the build folder available for webpack. This is done in the webpack config files by referencing the buildSubDirectory! Check it out in the index.js and throughout all other webpack.config files!


# Prerequisite - Commands that have to be executed to ensure a working project



# Running the webserver:

npm run dev -> runs the webserver



# Setting up Ganache

For development purposes it is okay to run ganache-cli with:

ganache-cli -a 15 -e 200 -d

This will generate the same 15 addresses with 200 ETH over and over again, because auf -d (--deterministic)



# Setting up the FileUploader

in /file-upload-hapi-master trigger:

yarn start

This will take care for the serverside upload of Tracks.
For further information on how Hapi.Js is working follow this documentation: https://scotch.io/bar-talk/handling-file-uploads-with-hapi-js



# Setting up the IPFS/ IPNS upload

In /file-upload-hapi-master/uploads execute this command:

python3 httprequestHandler.py

This script is responsible for uploading all files to IPFS. In our special case we use IPNS, so that we can reference to the same parent folder even after the hash itself changed



# Starting the ipfs daemon

ipfs daemon

Will run the local ipfs node on which all tracks are stored.




> A dApp that runs with truffle and uses VueJS
